# README #

* React, - Webpack, - Redux

Screenshot:


![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1503068396/Screen_Shot_2017-08-18_at_5.59.25_PM_raonxs.png)


TASK:

** Create the first javascript powered drone


* Your drone should be controlled using the arrow keys, each arrow key press (up/right/down/left) will advance your drone position by a single step.

* The initial position of your drone should be set to the center of the screen

** Control panel:


* At the top right corner, display your drone�s control panel
* The panel should display:
* The current X and Y position of your drone
* Fuel % status (each step of movement reduces 1% of the drones' fuel)

The code should be written with the help of ReactJS and ES6
Cover your code with tests


