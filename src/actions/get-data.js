export function loadData (data) {

    return {
        type: 'FUEL_UNLOADED',
        payload: data
    }
}
