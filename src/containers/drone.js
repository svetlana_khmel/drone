import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as getData from '../actions/get-data';
import { bindActionCreators } from 'redux';

const STEP = 10;
const FUEL_STEP = 1;

class Drone extends Component {
    constructor (props) {
        super();

        this.reduceFuel = this.reduceFuel.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.setCoords = this.setCoords.bind(this);

        this.state = {
            droneTop: window.innerHeight/2-70,
            droneLeft: window.innerWidth/2-100
        }
    }

    componentDidMount () {
        document.body.addEventListener('keydown', this.handleKeyPress);
        this.setCoords ();
    }

    componentWillUnmount () {
        document.body.removeEventListener('keydown', this.handleKeyPress);
    }

    reduceFuel () {
        const  getData  = this.props.getData.loadData;

        let data = {
            fuel: this.props.fuel-FUEL_STEP
        };

        getData(data);
    }

    setCoords () {
        const  getData  = this.props.getData.loadData;

        let data = {
            fuel: this.props.fuel,
            top: this.state.droneTop,
            left: this.state.droneLeft
        };
        getData(data);
    }

    handleKeyPress (event) {
        this.reduceFuel();

        if(event.key == 'ArrowUp'){
            let updatedTop =  this.state.droneTop - STEP;

            this.setState({
                droneTop: updatedTop
            });
        }

        if(event.key == 'ArrowDown'){
            let updatedTop =  this.state.droneTop + STEP;

            this.setState({
                droneTop: updatedTop
            });
        }

        if(event.key == 'ArrowRight'){
            let updatedLeft =  this.state.droneLeft + STEP;

            this.setState({
                droneLeft: updatedLeft
            });
        }

        if(event.key == 'ArrowLeft'){
            let updatedLeft =  this.state.droneLeft - STEP;

            this.setState({
                droneLeft: updatedLeft
            });
        }

        this.setCoords ();
    }

    render () {
        var droneStyle = {
            top: this.state.droneTop,
            left: this.state.droneLeft
        };

        return (
            <div className="drone-container" style={droneStyle}>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        fuel: state.fuel,
        top: state.top,
        left: state.left
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getData: bindActionCreators(getData, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Drone)