let initialState = {
    fuel: 100,
    top: 0,
    left: 0
};

export default function (state = initialState, action) {

    switch (action.type) {
        case 'FUEL_UNLOADED':
            return {
                ...state,
                fuel : action.payload.fuel,
                top : action.payload.top,
                left : action.payload.left
            };

        default:
            return state
    }

}