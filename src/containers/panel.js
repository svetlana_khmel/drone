import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as getData from '../actions/get-data';
import { bindActionCreators } from 'redux';

class Panel extends Component {
    constructor (props) {
        super();
    }

    render () {
        return  (
            <div className="control-panel">
                <div className="current-position-top">top: {this.props.top}</div>
                <div className="current-position-left">left: {this.props.left}</div>
                <div className="fuel-state">fuel: {this.props.fuel}%</div>
            </div>
        )
    };
}

function mapStateToProps (state) {
    return {
        fuel: state.fuel,
        top: state.top,
        left: state.left
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getData: bindActionCreators(getData, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Panel)