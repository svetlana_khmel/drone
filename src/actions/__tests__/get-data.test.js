import * as actions from '../get-data'

describe('actions', () => {
    it('should create an action to FUEL_UNLOADED', () => {
        let initialState = {
            fuel: 100,
            top: 0,
            left: 0
        };
        const expectedAction = {
            type: 'FUEL_UNLOADED',
            payload: initialState
        };
        expect(actions.loadData(initialState)).toEqual(expectedAction)
    })
});