import React, { Component } from 'react';
import Panel from './panel';
import Drone from './drone';

export default class App extends Component {

    render () {
        return (
           <div className="game-container">
               <Panel/>
               <Drone/>
           </div>
        )
    }
}
