import reducer from '../data-reducer';

const initial = {
    fuel: 100,
    top: 0,
    left: 0
};

describe('data reducer', () => {
    it('should return the initial state', () => {

        expect(reducer(undefined, {})).toEqual(
            {
                fuel: 100,
                top: 0,
                left: 0
            }
        )
    });

    it('should handle ADD_TODO', () => {
        expect(
            reducer({}, {
                type: 'FUEL_UNLOADED',
                payload: {
                    fuel: 99,
                    top: 0,
                    left: 0
                }
            })
        ).toEqual(
            {
                fuel: 99,
                top: 0,
                left: 0
            }
        )
    })
});

